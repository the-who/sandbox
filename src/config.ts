import { NetworkType } from '@airgap/beacon-sdk';
import { Factories, Token } from '@quipuswap/sdk';
import { IConfig } from './types';

export const MILLION = 1_000_000;
export const TOTAL_CLT_IN_POOL = 10_000_000;

const VITE_FA12_Q_FACTORY = import.meta.env.VITE_FA12_Q_FACTORY as string || 'KT1HrQWkSFe7ugihjoMWwQ7p8ja9e18LdUFn';
const VITE_FA2_Q_FACTORY = import.meta.env.VITE_FA2_Q_FACTORY as string || 'KT1Dx3SZ6r4h2BZNQM8xri1CtsdNcAoXLGZB';
const VITE_TOKEN_FA12 = import.meta.env.VITE_TOKEN_FA12 as string || 'KT1Sw95x26Fnh4hApBvMCtqv2DNxNzTUKB38';
const VITE_TOKEN_FA2 = import.meta.env.VITE_TOKEN_FA2 as string || 'KT19AQbd4rZ3e6B224YybenPQzAFDzGM2jRn';
const VITE_DEX_FA12 = import.meta.env.VITE_DEX_FA12 as string || 'KT1MyaQfe1mTmfAPXBnJbWvt1pgCCwaW7zdq';
const VITE_DEX_FA2 = import.meta.env.VITE_DEX_FA2 as string || 'KT1JquxMFsUZ9RyUp3w1u1iE4vLbNS1Hs99Z';
const VITE_FACTORY_FA12_S2Qv3 =
    import.meta.env.VITE_FACTORY_FA12_S2Qv3 as string || 'KT1RgVw9KYkZ4EJADCMFDFsTu9QXDLBLQJ1B';
const VITE_FACTORY_FA2_S2Qv3 =
        import.meta.env.VITE_FACTORY_FA2_S2Qv3 as string || 'KT1SsuN48WUQdBoApgpiaq375yfgXHiERM4h';
const VITE_POOL_FA12_S2Qv3 = import.meta.env.VITE_POOL_FA12_S2Qv3 as string || 'KT1KTSPQuWDyqtRqzWH8xtM84F1zRPSbnaeR';
const VITE_POOL_FA2_S2Qv3 = import.meta.env.VITE_POOL_FA2_S2Qv3 as string || 'KT1Q9txwmjBscKGuSzWUNP6jBNF4UbioyDEk';

export const allContracts = [
    VITE_FA12_Q_FACTORY,
    VITE_FA2_Q_FACTORY,
    VITE_TOKEN_FA12,
    VITE_TOKEN_FA2,
    VITE_DEX_FA12,
    VITE_DEX_FA2,
    VITE_FACTORY_FA12_S2Qv3,
    VITE_FACTORY_FA2_S2Qv3,
    VITE_POOL_FA12_S2Qv3,
    VITE_POOL_FA2_S2Qv3,
];

// Test tokens. Should be changed every token contract re-deployment
const testFA12Token: Token = {
    contract: VITE_TOKEN_FA12,
};
const testFA2Token: Token = {
    contract: VITE_TOKEN_FA2,
    id      : 0,
};
const testFA12QuipuShares: Token = {
    contract: VITE_DEX_FA12,
};
const testFA2QuipuShares: Token = {
    contract: VITE_DEX_FA2,
    id      : 0,
};


const config: IConfig = {
    endpoint     : 'https://hangzhounet.api.tez.ie',
    network      : 'hangzhounet',
    contract     : 'KT1DQ1hfbQMRhHRLupYUK7dHWBrHLxc8jnN4',
    tzktapi      : 'https://api.tzkt.io',
    v1           : 'KT1EH8yKXkRoxNkULRB1dSuwhkKyi5LJH82o',
    walletOptions: {
        name            : 'TheWhoTTery',
        iconUrl         : 'https://thewho.com/favicon.png',
        preferredNetwork: 'hangzhounet' as NetworkType,
    },
    app: {
        // Our factories for Quipu. Should be changed after each contract re-deployment
        factories: {
            fa1_2Factory: VITE_FACTORY_FA12_S2Qv3,
            fa2Factory  : VITE_FACTORY_FA2_S2Qv3,
        } as Factories,
        // Token ticker to token object mapping. XTZ_TOKEN - representation of Quipu shares tokens for the token
        tickerToToken: {
            AAA    : testFA12Token,
            BBB    : testFA2Token,
            XTZ_AAA: testFA12QuipuShares,
            XTZ_BBB: testFA2QuipuShares,
        } as { [ticker: string]: Token },
        nonDecimalTokens: ['AAA', 'BBB'],
        // Quipuswap factories. Should NOT be changed until we move to Token-Token Pool's or quipu completely moves to another contracts
        quipuFactories  : {
            fa1_2Factory: VITE_FA12_Q_FACTORY,
            fa2Factory  : VITE_FA2_Q_FACTORY,
        } as Factories,
    },
};

export const columns: any[] = [
    { name: 'id', align: 'center', label: 'ID in pool', field: 'id' },
    {
        name    : 'pool',
        required: true,
        label   : 'Pool',
        align   : 'left',
        field   : (row: any) => row.pool,
        format  : (val: any) => `${val}`,
        sortable: true,
    },
    { name: 'balance', align: 'center', label: 'Provided amount', field: 'balance', sortable: true },
    { name: 'reward', align: 'center', label: 'Winned in pool', field: 'reward' },
    { name: 'clt', align: 'center', label: 'CLT (% from total)', field: 'clt' },
    { name: 'totalwtt', align: 'center', label: 'Total WTT', field: 'totalwtt' },
    { name: 'roundwtt', align: 'center', label: 'Round WTT', field: 'roundwtt' },
    // { name: 'sodium', label: 'Sodium (mg)', field: 'sodium' },
    // { name    : 'calcium', label   : 'Calcium (%)', field   : 'calcium', sortable: true, 
    //     sort    : (a: any, b: any) => parseInt(a, 10) - parseInt(b, 10),
    // },
    // { name    : 'iron', label   : 'Iron (%)', field   : 'iron', sortable: true,
    //     sort    : (a: any, b: any) => parseInt(a, 10) - parseInt(b, 10) },
];

export default config;
