import { createRouter, createWebHashHistory } from 'vue-router';
import Home from './components/Home.vue';
import Profile from './components/Profile.vue';
import Admin from './components/Admin.vue';

const routes = [
    { path: '/', component: Home },
    { path: '/profile', component: Profile },
    { path: '/admin', component: Admin },
    { path: '/:pathMatch(.*)*', redirect: '/' },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

export default router;
