import { Factories, FoundDex, Token } from '@quipuswap/sdk';
import BigNumber from 'bignumber.js';
import { FoundPool } from './common/libs/loto/helpers';


export interface IAppConfig {
  factories: Factories;
  tickerToToken: { [ticker: string]: Token };
  nonDecimalTokens: string[];
  quipuFactories: Factories;
}
export interface IConfig {
  app: IAppConfig;
  [key: string]: any;
}
export interface IPoolStorage {
  token_id: any;
  ledger: any;
  id_to_addr: any;
  round_winners: any;
  total_users: BigNumber;
  farm_contract: string;
  token_contract: string;
  prn_src: string;
  round_timeframe: BigNumber;
  round_id: BigNumber;
  round_start: any;
  total_asset_pool: BigNumber;
  tezos_pool: BigNumber;
  token_pool: BigNumber;
  last_date_provided: any;
  last_date_withdrawn: any;
  round_acc_wtt: BigNumber;
  total_wtt: BigNumber;
  total_clt: BigNumber;
  next_user_id: BigNumber;
  reward_rec_contract: string;
  before_claim: BigNumber;
}

export interface IPoolAccount {
  user_id: BigNumber;
  balance: BigNumber;
  last_date_provided: any;
  last_date_withdrawn: any;
  reward: BigNumber;
  win_rounds: any;
  wtt_balance: {
    total_wtt: BigNumber;
    wtt_amt: BigNumber;
    round_id: BigNumber;
  };
  clt_balance: BigNumber;
  clt_claimed: BigNumber;
  last_clt_request: any;
}
export interface IApp {
  init: boolean;
  contract: null | string;
  signer: any;
  userAddress: null | string;
  config: IConfig;
  balance: null | { [ticker: string]: any };
  dexes: null | { [ticker: string]: FoundDex };
  pools: null | { [ticker: string]: FoundPool };
  winners: null | { [pool: string]: any };
  userData: null | { [pool: string]: IPoolAccount };
}
