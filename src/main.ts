import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import Buffer from 'buffer';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import { store } from './store';

import { Quasar, Notify } from 'quasar';
import '@quasar/extras/roboto-font/roboto-font.css';
import '@quasar/extras/material-icons/material-icons.css';
import 'quasar/src/css/index.sass';

(window as any).Buffer = Buffer.Buffer;

const app = createApp(App);


app.use(Quasar, {
    plugins: { Notify },
});
app.use(router);
app.use(store);

app.mount('#app');
