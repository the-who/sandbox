import { createStore } from 'vuex';
import config, { allContracts, MILLION } from '../config';
import { BeaconWallet } from '@taquito/beacon-wallet';
import {
    TezosToolkit,
    OpKind,
    compose,
    MichelCodecPacker
} from '@taquito/taquito';
import { Tzip16Module, tzip16 } from '@taquito/tzip16';
import { Tzip12Module, tzip12 } from '@taquito/tzip12';
import { RpcClient, RpcClientCache } from '@taquito/rpc';
import { ContractsLibrary } from '@taquito/contracts-library';
// import { NetworkType } from '@airgap/beacon-sdk';
import { LotoPool } from '../common/libs/loto';
import {
    Factories, findDex, ReadOnlySigner, toContract, Token, FoundDex
} from '@quipuswap/sdk';
import { findPool, findPoolNonStrict, ValuesType } from '../common/libs/loto/lib';
import { NetworkType } from '@airgap/beacon-wallet';
import BigNumber from 'bignumber.js';
import { FoundPool } from '../common/libs/loto/helpers';
import { IApp } from '../types';
import { Notify } from 'quasar';

const wallet: BeaconWallet = new BeaconWallet(config.walletOptions);
const rpcClient = new RpcClient(config.endpoint);
const contractsLibrary = new ContractsLibrary();

const tezos = new TezosToolkit(new RpcClientCache(rpcClient, 5000));

// New LotoPool object created for each token selection
export const loto = new LotoPool({
    tt: tezos,
});

tezos.addExtension(new Tzip16Module());
tezos.addExtension(new Tzip12Module());

// tezos.setProvider({ wallet });

const subscribeOperation = tezos.stream.subscribeOperation({
    and: [
        { destination: config.contract }, // must be our action contract
        { kind: OpKind.TRANSACTION },
    ],
});

const pool: any = {};

async function getContract(kt: any) {
    if (!pool[kt]) {
        console.log('not in a pool', kt);
        pool[kt] = tezos.wallet.at(kt, compose(tzip16, tzip12));
    }

    return pool[kt];
}

// Немного тупо извлекать storage, опираясь на тикер токена в приложении, но чтоб работало - сойдет пока
export const getTokenLedger = async(tokenName: string, storage: any) => {
    if (tokenName.startsWith('XTZ_')) return storage.storage.ledger;
    else return storage.ledger;
};
export const getProvidedSharesAmount = async(pool: FoundPool, userAddress: string) => {
    const account = await pool.storage.storage.ledger.get(userAddress);

    return account ? account.balance : new BigNumber(0);
};

export function delay(ms: number, result: any = true) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(result);
        }, ms);
    });
}

export const getContractCodeAndEps = async(address: string) => {
    const script = await tezos.rpc.getNormalizedScript(address);
    const entrypoints = await tezos.rpc.getEntrypoints(address);

    return { script, entrypoints };
};

export const getAllContracts = async(contracts: string[]) => {
    const result: { [contractAddress: string]: any } = {};
    const reqs = contracts.map(async(contractAddress) => {
        const obj = await getContractCodeAndEps(contractAddress);

        result[contractAddress] = obj;
    });
    
    await Promise.all(reqs);

    return result;
};

// Create a new store instance.
const store = createStore({
    state(): IApp {
        return {
            init       : false,
            contract   : null,
            signer     : null,
            userAddress: null,
            config,
            balance    : null,
            dexes      : null,
            pools      : null,
            winners    : null,
            userData   : null,
        };
    },
    getters: {
        isConnected(state: IApp) {
            return !!state.userAddress;
        },
        hasContractsData(state: IApp) {
            return state.dexes && state.pools;
        },
    },
    actions: {
        async init({ commit, dispatch }) {
            const contractObjects = await getAllContracts(allContracts);
            
            tezos.addExtension(contractsLibrary);

            contractsLibrary.addContract(contractObjects);
            await dispatch('getInitData');

            const activeAccount = await wallet.client.getActiveAccount();

            if (activeAccount) await dispatch('handleActiveAccount', activeAccount);

            commit('initDone');
        },

        getInitData({ dispatch }) {
            const reqs = [
                dispatch('getContract'),
                dispatch('getDexes'),
                dispatch('getPools'),
            ];

            return Promise.all(reqs);
        },

        getContract({ commit }) {
            return getContract(config.contract)
                .then((contract) => {
                    commit('contract', contract);
                });
        },

        getDexes({ commit }) {
            const reqs = [];
            const quipuFactories = config.app.quipuFactories; // Should be defined to lookup for quipu DEX'es for token's
            const dexes: {[key: string]: FoundDex} = {};

            for (const key in config.app.tickerToToken) {
                const splittedKey = key.split('_');
                const ticker = splittedKey.length > 1 ? splittedKey[1] : key;

                reqs.push(
                    findDex(tezos, quipuFactories, config.app.tickerToToken[ticker])
                        .then((data) => {
                            dexes[key] = data;
                        })
                );
            }

            return Promise.all(reqs)
                .then(() => {
                    commit('setDexes', dexes);
                });
        },

        getPools({ commit }) {
            const reqs = [];
            const factories: Factories = config.app.factories; // Should be defined to lookup for our pool's for token's
            const pools: {[key: string]: FoundPool} = {};

            for (const key in config.app.tickerToToken) {
                const splittedKey = key.split('_');
                const ticker = splittedKey.length > 1 ? key : `XTZ_${key}`;

                reqs.push(
                    findPool(tezos, factories, config.app.tickerToToken[ticker])
                        .then((data) => {
                            pools[key] = data;
                        })
                );
            }

            return Promise.all(reqs)
                .then(() => {
                    commit('setPools', pools);
                });
        },

        async handleActiveAccount({ commit, dispatch }, activeAccount) {
            const address = await wallet.getPKH();

            tezos.setWalletProvider(wallet);

            tezos.setSignerProvider(
                new ReadOnlySigner(activeAccount.address, activeAccount.publicKey)
            );

            commit('userAddress', address);

            dispatch('getBalance');
            dispatch('getUserData');

            console.log('account handled');

            return true;
        },

        getUserData({ state, commit }) {
            const reqs = [];
            const userData: any = {};

            for (const key in config.app.tickerToToken) {
                const splittedKey = key.split('_');
                const ticker = splittedKey.length > 1 ? key : `XTZ_${key}`;
                const pool = state.pools && state.pools[ticker];

                if (pool) {
                    reqs.push(
                        pool.storage.storage.ledger.get(state.userAddress)
                            .then((ud: any) => {
                                if (ud) userData[key] = ud;
                            })
                    );
                }
            }

            return Promise.all(reqs).then(() => {
                commit('setUserData', userData);
            });
        },

        // Another approach should be used there. This is just example
        // In case of large amount of tokens balances loading could stuck
        // I think we can pass token name instead of debugData and fetch token balance only when select action done by user
        // As far as we have only 2 token's for now we can leave this implementation
        async getBalance({ state, commit }, debugData = { AAA: 0, BBB: 0, XTZ: 0, XTZ_AAA: 0, XTZ_BBB: 0 }) {
            const activeAccount = await wallet.client.getActiveAccount();

            if (!activeAccount) {
                commit('setBalance', {});

                console.log('no wallet');

                return;
            }

            // Fetch amount of tezos at user wallet
            const tezBalance = await tezos.tz.getBalance(activeAccount.address);

            const result: {[ticker: string]: BigNumber} = {
                XTZ: tezBalance.dividedBy(MILLION),
            };

            const tokens = Object.keys(config.app.tickerToToken);

            // Get balance by token name and fill result object
            const fillResultWithBalances = async(tokenName: string) => {
                const isDecimal = !state.config.app.nonDecimalTokens.includes(tokenName);
                const divider = isDecimal ? 10 ** 6 : 1;
                // Get token object from config
                const tokenObj = config.app.tickerToToken[tokenName];
                let bal = 0;

                try {
                    // Fetch token contract object
                    const tokenContract = await toContract(tezos, tokenObj.contract);
                    // Fetch contract storage
                    const storage = await tokenContract.storage<any>();
                    const ledger = await getTokenLedger(tokenName, storage);
                    // Fetch value from BigMap "ledger". In both case (FA12/FA2) balance stored in ledger BigMap of (address => { balance: nat, ...other data })
                    const balanceObj = await ledger.get(activeAccount.address);

                    bal = balanceObj ? balanceObj.balance : 0;

                    if (isDecimal && state.userAddress && state.pools && state.pools[tokenName]) {
                        const pool = state.pools[tokenName];
                        const b = await getProvidedSharesAmount(pool, state.userAddress);

                        result[`POOL_${tokenName}`] = new BigNumber(b).dividedBy(divider);
                    }
                } catch (e) {
                    console.error(`${tokenName}`, e);
                } finally {
                    result[tokenName] = new BigNumber(bal).dividedBy(divider);
                }
            };

            await Promise.all(
                tokens.map(token => fillResultWithBalances(token))
            );

            commit('setBalance', result);

            return;
        },

        async connectWallet({ state, dispatch }) {
            let activeAccount = await wallet.client.getActiveAccount();

            if (!activeAccount) {
                console.log('No active account. Requesting permissions');
                console.log(wallet);

                await wallet.client.requestPermissions({
                    network: {
                        type  : config.network as NetworkType,
                        rpcUrl: config.endpoint,
                    },
                });

                console.log(wallet);

                activeAccount = await wallet.client.getActiveAccount();
                // console.log(tezos);

                if (!activeAccount)
                    throw new Error('Wallet not connected');
            }

            return dispatch('handleActiveAccount', activeAccount);
        },

        disconnectWallet({ commit }) {
            if (wallet) {
                wallet.clearActiveAccount().then(() => {
                    commit('userAddress', null);
                    commit('setBalance', null);
                    commit('setUserData', null);
                });
            }
        },

        enterPool({ dispatch, state }, data) {
            if (state.userAddress) {
                return dispatch('placePair', data);
            } else {
                return dispatch('connectWallet')
                    .then(() => dispatch('placePair', data));
            }
        },

        async placePair({ state, dispatch }, data: {
          XTZ: number;
          [tokenName: string]: number;
        }) {
            if(!state.pools || !state.dexes) return;

            const tokenName = Object.keys(data).find(n => n !== 'XTZ') || 'AAA'; // Token ticker selected by user

            //#region calculate estimates example
            /*
            const factories: Factories = config.app.factories; // Should be defined to lookup for our pool's for token's
            const quipuFactories = config.app.quipuFactories; // Should be defined to lookup for quipu DEX'es for token's
            const token: Token = config.app.tickerToToken[tokenName]; // Token object
            // Dex object is necessary to calculate estimates
            const [dex, pool] = await Promise.all([
                findDex(tezos, quipuFactories, token),
                findPool(tezos, factories, token),
            ]);
            // If user provided only XTZ amount, we do estimateTokenInTez
            const tezValue = 10_000_000; // Tezos value to send in mutez(1 XTZ === 1_000_000 mutez)
            const tokenValue = estimateTokenInTez(dex.storage, tezValue); // should calculate tokenValue as BigNumber
            */
            /*
            // If user provided only tokenValue, wo need to estimateTezInToken
            const tokenValue = 400;
            const tezValue = estimateTezInToken(dex.storage, tokenValue);
            */
            //#endregion
            const tezValue = new BigNumber(data.XTZ).multipliedBy(MILLION); // Tezos amount to send in mutez(1 XTZ === 1_000_000 mutez)
            const tokenValue = new BigNumber(data[tokenName]).multipliedBy(MILLION); // Tokens amount to send

            const pool = state.pools[tokenName];
            const valuesObj: ValuesType = { tezValue, tokenValue };
            let op;

            if (tezValue.isZero()) {
                op = await loto.enterQuipuPool(pool, tokenValue);
            } else {
                const dex = state.dexes[tokenName];

                if (tokenName === 'AAA' || tokenName === 'BBB')
                    valuesObj.tokenValue = new BigNumber(valuesObj.tokenValue).idiv(MILLION); // AAA и BBB токены не имеют decimals, поэтому делаем так

                op = await loto.enterQuipuPoolWithXtzAndToken(pool, dex, valuesObj);
            }

            const notif = Notify.create({
                group  : false, // required to be updatable
                timeout: 0, // we want to be in control when it gets dismissed
                spinner: true,
                color  : 'primary',
                message: 'Entering pool...',
            });

            await op.confirmation(); // Confirmation awaiting. Can take up to 30 sec's in mainnet and more in testnet

            notif({
                icon   : 'done', // we add an icon
                spinner: false, // we reset the spinner setting so the icon can be displayed
                message: 'Success!',
                color  : 'positive',
                timeout: 2500, // we will timeout it in 2.5s
            });

            dispatch('getBalance');
            dispatch('getUserData');

            return;
        },

        async withdraw({ dispatch, state }, data) {
            if (state.userAddress) {
                return await dispatch('withdrawPair', data);
            } else {
                return dispatch('connectWallet')
                    .then(() => dispatch('withdrawPair', data));
            }
        },

        async withdrawPair({ state, dispatch }, data: {
          XTZ: number;
          [tokenName: string]: number;
        }) {
            if(!state.pools || !state.dexes) return;

            const tokenName = Object.keys(data).find(n => n !== 'XTZ') || 'AAA'; // Token ticker selected by user

            const tezValue = new BigNumber(data.XTZ).multipliedBy(MILLION); // Tezos amount to send in mutez(1 XTZ === 1_000_000 mutez)
            const tokenValue = new BigNumber(data[tokenName]).multipliedBy(MILLION); // Tokens amount to send

            const pool = state.pools[tokenName];
            // const valuesObj: ValuesType = { tezValue, tokenValue };
            const op = await loto.exitPool(pool, tokenValue);

            const notif = Notify.create({
                group  : false, // required to be updatable
                timeout: 0, // we want to be in control when it gets dismissed
                spinner: true,
                color  : 'primary',
                message: 'Withdrawing...',
            });

            await op.confirmation(); // Confirmation awaiting. Can take up to 30 sec's in mainnet and more in testnet

            notif({
                icon   : 'done', // we add an icon
                spinner: false, // we reset the spinner setting so the icon can be displayed
                message: 'Success!',
                color  : 'positive',
                timeout: 2500, // we will timeout it in 2.5s
            });

            dispatch('getBalance');
            dispatch('getUserData');

            return;
            // return delay(1500, { result: true })
            //     .then(() => dispatch('getBalance', {}));
        },
    },
    mutations: {
        initDone(state: IApp) {
            state.init = true;
        },
        contract(state, contract) {
            state.contract = contract;
        },
        userAddress(state, address) {
            state.userAddress = address;
        },
        setBalance(state, v) {
            state.balance = v;
        },
        setDexes(state, v) {
            state.dexes = v;
        },
        setPools(state, v) {
            state.pools = v;
        },
        setUserData(state, v) {
            state.userData = v;
        },
    },
});

export default store;
