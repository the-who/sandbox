import BigNumber from 'bignumber.js';
import { MILLION } from './config';
import { IPoolAccount, IPoolStorage } from './types';

export const mapUserDataToRows = (userData: any, ticker: string, pools: any) => {
    const ud: IPoolAccount = userData.value[ticker];
    const pool: IPoolStorage = pools.value[ticker].storage.storage;

    if (!ud) return null;

    const totalwtt = ud.wtt_balance.total_wtt.toString();
    const roundwtt = ud.wtt_balance.round_id.isEqualTo(pool.round_id) ? ud.wtt_balance.wtt_amt.toString() : '1';
    const cltAmt = ud.clt_balance.toString();
    const cltShare = ud.clt_balance.dividedBy(pool.total_clt).multipliedBy(100);
    const clt = `${cltAmt} CLT (${cltShare}%)`; // TODO: 1. добавить тут иконку токена. 2. может можно как-то лучше сделать
    const rewAmt = ud.reward.dividedBy(MILLION);

    const reward = rewAmt.isZero() ? '-' : `${rewAmt.toString()} XTZ`;

    const obj = {
        pool   : ticker,
        id     : ud.user_id.toString(),
        balance: ud.balance.dividedBy(MILLION).toString(),
        clt,
        reward,
        totalwtt,
        roundwtt,
    };

    return obj;
};

export const getTotals = (userData: { [ticker: string]: IPoolAccount }) => {
    const total = Object.keys(userData).reduce(
        (acc, cur) => {
            const poolUserData = userData[cur];

            if (!poolUserData || cur.startsWith('XTZ_')) return acc;

            const rewAmt: BigNumber = poolUserData.reward.dividedBy(MILLION);
            const inPoolAmt: BigNumber = poolUserData.balance.dividedBy(MILLION);
            const accReward = acc.rew || new BigNumber(0);
            const accIpPool = acc.inPool || new BigNumber(0);

            return {
                rew         : accReward.plus(rewAmt),
                inPool      : accIpPool.plus(inPoolAmt),
                enteredPools: rewAmt.isZero() ? acc.enteredPools : ++acc.enteredPools,
            };
        }, { enteredPools: 0 } as { rew: BigNumber; inPool: BigNumber; enteredPools: number });

    return total;
};
