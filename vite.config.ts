import vue from '@vitejs/plugin-vue';
import { defineConfig, mergeConfig } from 'vite';
import { quasar, transformAssetUrls } from '@quasar/vite-plugin';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path');

// eslint-disable-next-line max-lines-per-function
export default ({ command }: {command: string}) => {
    const isBuild = command === 'build';

    return defineConfig({
        build: {
            target         : 'esnext',
            commonjsOptions: {
                transformMixedEsModules: true,
            },
        },
        resolve: {
            alias: {
                // dedupe @airgap/beacon-sdk
                // I almost have no idea why it needs `cjs` on dev and `esm` on build, but this is how it works 🤷‍♂️
                '@airgap/beacon-sdk': path.resolve(
                    __dirname,
                    `./node_modules/@airgap/beacon-sdk/dist/${
                        isBuild ? 'esm' : 'cjs'
                    }/index.js`
                ),
                // polyfills
                'readable-stream': 'vite-compatible-readable-stream',
                stream           : 'vite-compatible-readable-stream',
            },
        },
        plugins: [
            vue({
                template: { transformAssetUrls },
            }),

            quasar({
                sassVariables: 'src/quasar-variables.sass',
            }),
        ],
        server: {
            https: true,
            port : '8080',
        },
    });
};
