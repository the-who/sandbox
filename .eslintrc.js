module.exports = {
    env: {
        node: true,
    },
    extends: [
        'plugin:vue/vue3-recommended',
        'eslint:recommended',
        '@vue/typescript/recommended',
    ],
    rules: {
    // override/add rules settings here, such as:
        'vue/no-unused-vars'                         : 'warn',
        'vue/singleline-html-element-content-newline': 'off',
        'vue/multi-word-component-names'             : 'off',
        '@typescript-eslint/no-explicit-any'         : 'off',
        '@typescript-eslint/no-implicit-any'         : 'off',
        'no-unused-vars'                             : 'warn',
        'prefer-const'                               : ['error'],
        'curly'                                      : [
            'error',
            'multi-or-nest',
            'consistent',
        ],
        'object-curly-spacing'           : ['error', 'always'],
        'space-before-function-paren'    : ['error', 'never'],
        'padding-line-between-statements': [
            'error',
            { 'blankLine': 'always', 'prev': '*', 'next': 'return' },
            { 'blankLine': 'always', 'prev': ['const', 'let', 'var'], 'next': '*' },
            { 'blankLine': 'any',    'prev': ['const', 'let', 'var'], 'next': ['const', 'let', 'var'] },
            { 'blankLine': 'always', 'prev': ['if', 'case', 'default'], 'next': '*' },
            { 'blankLine': 'always', 'prev': '*', 'next': ['if', 'case', 'default'] },
            { 'blankLine': 'any',    'prev': ['if', 'case', 'default'], 'next': ['if', 'case', 'default'] },
        ],
        'max-params'    : ['warn', 4],
        'indent'        : ['warn', 4],
        'semi'          : ['error', 'always'],
        'max-statements': ['warn', 25, { 'ignoreTopLevelFunctions': true }],
        'key-spacing'   : [1, {
            'singleLine': {
                'beforeColon': false,
                'afterColon' : true,
            },
            'multiLine': {
                'align': 'colon',
            },
        }],
        'comma-dangle': ['error', {
            'arrays'   : 'always-multiline',
            'objects'  : 'always-multiline',
            'imports'  : 'never',
            'exports'  : 'never',
            'functions': 'never',
        }],
        'complexity'              : ['warn', 10],
        'no-param-reassign'       : ['error', { 'props': false }],
        'prefer-arrow-callback'   : 'warn',
        'no-case-declarations'    : 'error',
        'max-lines-per-function'  : ['error', { 'max': 50, 'skipComments': true, 'skipBlankLines': true }],
        'object-shorthand'        : ['error', 'always'],
        'quotes'                  : ['error', 'single', { 'avoidEscape': true }],
        'arrow-body-style'        : ['warn', 'as-needed'],
        'newline-per-chained-call': ['warn', { 'ignoreChainWithDepth': 3 }],
        'func-name-matching'      : ['warn', { 'considerPropertyDescriptor': true }],
        //------------------------------------------------------------------------------
        // Vue rules
        //------------------------------------------------------------------------------
        'vue/html-indent'         : ['warn', 4],
        'vue/script-indent'       : ['warn', 4],
        'vue/max-len'             : ['error', {
            'code'                  : 120,
            'template'              : 140,
            'ignorePattern'         : '',
            'ignoreComments'        : true,
            'ignoreTrailingComments': false,
            'ignoreUrls'            : true,
        }],
        'vue/no-useless-concat': 'warn',
        //------------------------------------------------------------------------------
    },
};
